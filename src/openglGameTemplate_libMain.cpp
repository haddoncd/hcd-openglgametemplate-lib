#ifdef openglGameTemplate_libMain_cpp
#error Multiple inclusion
#endif
#define openglGameTemplate_libMain_cpp

#include "common.cpp"
#include "MemoryStack.cpp"
#include "Str.cpp"
#include "StrBuf.cpp"
#include "vector.hpp"
#include "matrix.hpp"
#include "args.cpp"
#include "ScopeExit.hpp"

#include "Gl3wFunctions.hpp"
#include "Gl3wFunctions_util.cpp"
#include "openglGame_common.cpp"

#include "assetManager_config_assetType.hpp"
#include "openglGame_AssetLabel.hpp"

#include "magicaVoxel_Mesh.hpp"
#include "shaders_glsl.cpp"
#include "ImguiAdaptor.hpp"

#include "Assets.cpp"
#include "openglGameTemplate.cpp"
#include "openglGame_exports.cpp"
